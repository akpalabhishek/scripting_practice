#! /bin/bash
# this is a comment
# echo "Hello World"

# echo Our shell name is $BASH # variable
# echo Our shell version is $BASH_VERSION
# echo Our current directory is $PWD
# echo our home directory is $HOME

# name=ABHI
# 10val=10 # Variable doesn't start with number's
# echo The name is $name
# echo value $10val

# =====Read user input =====

# echo "Enter name's: "
# read name1 name2 name3
# echo "Entered name is $name1, $name2, $name3"

# read -p 'username : ' user_var
# read -sp 'password : ' pass_var
# echo
# echo "username : $user_var"
# echo "password : $pass_var"

# echo "Enter names : "
# read -a names
# echo "Names : ${names[0]}, ${names[1]}"

# echo "Enter name : "
# read
# echo "Name : $REPLY"

# ====== Pass argument to a bash-script======

# echo $0 $1 $2 $3 '> echo $1 $2 $3'
# args=("$@")
# # echo ${args[0]} ${args[1]} ${args[2]} ${args[3]}
# echo $@
# echo $# # no. of argument pass

# =========== If else Statement============
# count=10
# if [ $count -gt 9 ]
# if (($count > 9))
# word=a
# if [[ $word == "b" ]]
# then
#     echo "condition is true"
# elif [[ $word == "a" ]]
# then
#     echo "condition is true"
# else
#     echo "condition is false"
# fi

# ========file test operators===========
# echo -e "Enter the name of the file : \c" # \c = position of cursor -e = enable enterpreter
# read file_name

# if [ -f $file_name ] # -f for file and -d for dir for blog special file -b and character special file -c file empty -s for read write and execute rwx
# then
#     echo "$file_name found"
# else
#     echo "$file_name not found"
# fi

# ==========append output ot the end of file=========
# echo -e "enter the name of the file : \c"
# read file_name
# if [ -f $file_name ]
# then
#     if [ -w $file_name ]
#     then
#         echo "Type some text data. To quit press ctrl+d"
#         cat >> $file_name
#     else
#         echo "The file do not have write permissions"
#     fi
# else
#     echo "$file_name not exists"
# fi

# ====== Logical 'AND' Operator=============
#  age=25

#  if [ "$age" -gt 18 ] && [ "$age" -lt 30 ]
# if [ "$age" -gt 18 -a "$age" -lt 30 ]
# if [[ "$age" -gt 18 && "$age" -lt 30 ]]
# then
#     echo "valid age"
#     else
#     echo "age not valid"
# fi

# ==========Logical 'OR' operator========
# age=25
# if [ "$age" -eq 18 ] || [ "$age" -eq 30 ]
# if [ "$age" -eq 18 -o "$age" -eq 30 ]
# if [[ "$age" -eq 18 || "$age" -eq 30 ]]
# then
#     echo "valid age"
#     else
#     echo "age not valid"
# fi

# =======Perform airthmetic operation===========
# num1=20 
# num2=5
# echo $(( num1 + num2 ))
# echo $(( num1 - num2 ))
# echo $(( num1 * num2 ))
# echo $(( num1 / num2 ))
# echo $(( num1 % num2 ))
# echo
# echo $(expr $num1 + $num2)
# echo $(expr $num1 - $num2)
# echo $(expr $num1 \* $num2)
# echo $(expr $num1 / $num2)
# echo $(expr $num1 % $num2)

# =========floating poin math operation======
# num1=20.5
# num2=5
# echo "20.5+5" | bc
# echo "20.5-5" | bc
# echo "20.5*5" | bc
# echo "scale=2;20.5/5" | bc
# echo "20.5%5" | bc
# echo "$num1+$num2" | bc

# num=27
# echo "scale=2;sqrt($num)" | bc -l # for squire
# echo "scale=2;3^3" | bc -l # power of 3

# ======The case statement============
# vehicle=$1
# case $vehicle in
#     "car" )
#         echo "Rent for $vehicle is 100$" ;;
#     "van" )
#         echo "Rent for $vehicle is 80$" ;;
#     "bicycle" )
#         echo "Rent for $vehicle is 5$" ;;
#     "truck" )
#         echo "Rent for $vehicle is 150$" ;;
#     * )
#         echo "uknown vehicle" ;;
# esac

# echo -e "enter some character : \c"
# read value

# case $value in
#     [a-z] )
#         echo "User entered $value a to z" ;;
#     [A-Z] )
#         echo "User entered $value A to Z" ;; # set variable LANG=C then try
#     [0-9] )
#         echo "User entered $value 0 to 9" ;;
#     ? )
#         echo "User entered $value special chracter" ;;
#     * )
#         echo "uknown input" ;;
# esac

# =================Array variables============
# os=('ubuntu' 'windows' 'Linux')
# os[3]='mac' # Add index
# os[0]='mac' # replace index
# unset os[2] # remove index
# echo "${os[@]}" 
# echo "${os[0]}"
# echo "${os[1]}"
# echo "${!os[@]}" # index of the array
# echo "${#os[@]}" # Leangth of the array

# string=asdsjskgsGLASvfdv
# echo "${string[@]}"
# echo "${string[0]}"
# echo "${string[1]}"
# echo "${#string[@]}"

# =========While Loop=============
# n=1
# # while [ $n -le 10 ]
# while (( $n <= 10 ))
# do
#     echo "$n"
#     # n=$(( n+1 ))
#     (( n++ ))
# done

# =====using sleep and open terminal with while loops=====
# n=1
# # while [ $n -le 10 ]
# while (( $n <= 3 ))
# do
#     echo "$n"
#     # n=$(( n+1 ))
#     (( n++ ))
#     # sleep 1
#     cmd &
# done

# ====Read a file content in bash========
# cat hello.sh | while read p # when condition is true then run While loop
# while IFS= read -r line
# do
#     echo $line
# done < hello.sh

# ===========UNTIL loop=================
# n=1

# until [ $n -ge 10 ] # when condition is false then run Until loop
# do
#     echo $n
#     n=$(( n+1 ))
# done

# ============for loop==============
#  for (( i=0; i<5; i++ ))
#  do
#     echo $i
# done

# ======Use for loop to execute commands===========
# for command in ls pwd date
# do
#     echo "----------$commands------------"
#     $command
# done

# for item in *
# do
#     if [ -f $item ] # use -f for file and -d fro directory 
#     then
#         echo $item
#     fi
# done

# ====== Select Loop ============
# select name in mark john tom ben
# do
#     # echo "$name selected" # use for case
#     case $name in
#     mark )
#         echo mark selected 
#         ;;
#     john )
#         echo john selected 
#         ;;
#     tom )
#         echo tom selected 
#         ;;
#     ben )
#         echo ben selected 
#         ;;
#     * )
#         echo "Error please provide the no. between 1..4"  ;;
#     esac
# done

# ==========break and Continoue=============
# for (( i=0; i<10; i++ ))
# do
#     # if [ $i -gt 5 ]
#     if [ $i -eq 3 -o $i -eq 6 ]
#     then
#         # break
#         continue
#     fi
#     echo $i
# done

# ============== Function ===============
# function Hello(){
#     # echo "Hello World"
#     # echo $1 $2 $3
#     local name=$1 # variable works only in function
#     echo "The name is $name"
# }

# quit () {
#     exit
# }
# name="Tom"
# echo "The name is $name"

# # Hello print Hello again
# Hello Max

# echo "Foo"

# echo "The name is $name"

# ============function example=======
# usage() {
#     echo "you need to provide an argument : "
#     echo "usage : $0 file_name"
# }
# if_file_exist() {
#     local file="$1"
#     [[ -f $file ]] && return 0 || return 1
# }

# [[ $# -eq 0 ]] && usage

# if ( if_file_exist "$1" )
# then
#     echo "File found"
# else
#     echo "File not found"
# fi

# ===========Readonly command==========
# var=31
# readonly var

# var=50

# echo "var => $var"

# hello() {
#     echo "Hello World"
# }

# readonly -f hello

# hello() {
#     echo "Hello World Again"
# }

# readonly -f hello # -f for function -p for variable
# readonly -f

# =======Signals and Traps========
# echo "pid is $$"
# while (( COUNT < 10 ))
# do
#     sleep 10
#     (( COUNT ++ ))
#     echo $COUNT
# done
# exit 0
# file=/d/bash_practice/file.txt
# trap "rm -f $file && echo file deleted; exit" 0 2 15
# # trap "echo Exit command is detected" SIGKILL # can't cath SIGKILL and SIGSTOP

# echo "pid is $$"
# while (( COUNT < 10 ))
# do
#     sleep 10
#     (( COUNT ++ ))
#     echo $COUNT
# done
# exit 0
# remove trap from terminal trap - 0 2 15
# echo "Hello World"

# exit 0

# ==========debug a bash script=======
# bash -x ./hello.sh to debug
# on top
# !/bin/bash
# set -x # start debuging
# set +x # stop debuging