@echo off
REM set /a sum=5*5
REM set /a sum=10 / 5
REM set /a sum=10 %% 3
REM /a = mathematical expression
REM echo The sum is %sum%
REM echo ^<html^>
REM echo Please enter your name!
REM set /p name=
REM set /p name=Please enter you name! 
REM echo.
REM echo Your name is %name%
REM :another
REM cls
REM ver
REM :label
REM vol
REM echo This is very begining of the program!
REM goto :end

REM :start
REM echo this is the start of the program!
REM REM goto :middle

REM :middle
REM echo This is the MIDDLE of the program!


REM :end
REM echo This is the END of the program!
:: This is comment/label!

REM ===========functions=======
REM goto :main
REM REM echo This is very beginingof the program!
REM :function
REM     echo        This is another function!
REM goto :eof

REM :main
REM     echo        Main function is being called!
REM     call :function
REM     echo    End of main function!
REM     echo End of program!
REM goto :eof

REM goto :main
REM :say_something
REM     echo I am saying %~1 and %~2
REM goto :eof

REM :main
REM     echo This is the main function!
REM     call :say_something tiger boat
REM goto :eof

REM goto :main

REM :new_function

REM     echo        Changing a variable....

REM     set %~1=Lunch
REM goto :eof

REM :main
REM     echo This is the main function!
REM     set new_var=Dinner

REM     echo The variable new value is at first %new_var%
REM     call :new_function new_var

REM     echo The variable new value is at now %new_var%

REM goto :eof

REM :func
REM setlocal
REM     echo func says p is %p%
REM     set x=30
REM     echo func says X is %x%
REM endlocal
REM goto :eof

REM :main
REM setlocal
REM     echo The main function is being called!
REM     set p=4
REM     echo Main says P is %p%
REM     call :func
    
REM     echo Main says X is %x%
REM endlocal
REM goto :eof

REM goto :main

REM :main
REM setlocal
REM     echo Creating a variable...
REM     REM set global_var=existing
REM     set local_var=nonexistant
REM endlocal
REM goto :eof

REM goto :main

REM :add_one
REM setlocal
REM     set something=everything
REM     echo Performing add One on X...
REM endlocal & set /a x=%x%+1
REM goto :eof
REM :main
REM setlocal
REM     echo Main function is calling..
REM     echo setting X to 1....
REM     set /a x=1

REM     call :add_one

REM     echo The value of X is %x%
REM     echo.
REM     echo.
REM     echo something is %something%
REM endlocal
REM goto :eof

REM goto :main
REM :add_one
REM setlocal
REM     echo Runiing 'add_one'...
REM endlocal & set /a %~1=%~2+1
REM goto :eof

REM :main
REM setlocal
REM     set /a x=1
REM     set /a y=50
REM     echo Created variable X and set it to %x%
REM     echo.
REM     call :add_one y %y%
REM     echo.
REM     echo The value of Y is now %y%
REM endlocal
REM goto :eof

REM goto :main
REM :main
REM setlocal
REM     echo Hey!
REM     echo.
REM     set /a food=10
REM     set /a needed_food=50
REM     if %food%==%needed_food% (
REM         echo We have enough food.
REM     )else (
REM         echo We do not have enough food.
REM     )
REM     echo.
REM     echo GoodBye!
REM endlocal
REM goto :eof

REM goto :main
REM :main
REM setlocal
REM     set /a food=50
REM     set /a needed_food=5

REM     set /a people=10
REM     set /a ration=5
REM     set /a all_food=%people%*%ration%

REM     if %food% geq %needed_food% (
REM         echo We have a good amount of food. 
REM         if %all_food% leq %food% (
REM             echo We have enough food for all %people% people!
REM         )else (
REM             echo We do not have enough food for all these people!
REM         )
REM     )else (
REM         echo We do not have enough food.
REM     )
REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
REM     set /a counter=0
REM     set /a limit=10

REM     :loop
REM     if !counter! lss !limit! (
REM         echo !counter!
REM         set /a counter=!counter!+1
REM         goto :loop
REM     )
REM     echo.
REM     echo counter is now !counter!
REM     echo.
REM     echo outside of loop^^!

REM endlocal
REM goto: eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
REM     set string=Hello World^^!
REM     echo !string!
REM     echo.

REM     set /a test=3

REM     for %%g in ( 1 2 3 4 5 ) do (
REM         echo %%g
REM         REM if !test! equ %%g (
REM         REM     echo !test! is %%g
REM         REM )
REM     )

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
    
REM     for /l %%g in ( 0, 20, 100 ) do (
REM         echo %%g
REM     )

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal

REM     for %%g in ( * ) do (
REM         echo %%g
REM     )
REM REM /d for directory, /r for recusive, /f file info (type file name)
REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal

REM :: G H I J K

REM     for %%g "tokens=1,3,4 delims=, " %%g in ( banks.csv ) do (
REM         echo %%g, Who is %%h and banks at '%%i'
REM )

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal

REM     for /f "delims=/ eol=#" %%g in ( script.bat ) do (
REM         echo %%g
REM     )

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
REM     REM ex abhi kumar in path then use "delims=/"
REM     for /f %%g in ( 'cd' ) do (
REM         echo %%g
REM     )

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
REM     set string=This is a nice sentence
REM     for /f "tokens=1-9" %%g in ( "!string!" ) do (
REM         echo %%g %%h %%i
REM     )

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal

REM     for /l %%g in ( 1,1,10 ) do (
REM         if %%g equ 5 (
REM             goto :loop_end
REM         )
REM         echo %%g
REM     )
REM     :loop_end
REM     echo.
REM     echo End of Loop^^!

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
    
REM     dir /b > new_file.txt 2>&1

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
    
REM     echo y| choice

REM endlocal
REM goto :eof

REM prompt $_$C$S%USERNAME%@%COMPUTERNAME%$S$F$_$P$S$G$S REM out of this type prompt

REM setlocal enabledelayedexpansion
REM goto :main

REM :main

REM     set var=This is totally a string
REM     echo !var:~0,-6!
REM     echo !var:~4,7!

REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main

REM     set var=The cat in the hat ate the mat
REM     set var=!var:cat =!
REM     echo !var:hat =!

REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal

REM     set string="More Informstion, more words"
REM     for /f "useback tokens=*" %%g in ('!string!') do set string=%%~g
REM     echo !string!

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal

REM     call create_string var "this is some text"
REM     echo !var!
REM     echo !var_length!

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal

REM     call create_array grocery_list "," "Apples,Bananas,Meat"

REM     echo !grocery_list[1]!

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto: main

REM :main
REM setlocal

REM     call create_array list_of_numbers "," "45,523,2,65,7,-2,-34,1"

REM     call get_minimum list_of_numbers min
REM     echo !min!

REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
REM     call get_power 2 2 pow
REM     echo %pow%
REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
REM     call absolute_value 4 abs
REM     echo %abs%
REM endlocal
REM goto :eof

REM setlocal enabledelayedexpansion
REM goto :main

REM :main
REM setlocal
REM     call range numbers "," 15 30
REM     echo !numbers!
REM endlocal
REM goto :eof

setlocal enabledelayedexpansion
goto :main

:main
setlocal
    call create_array numbers " " "100 1 10"
    call sum numbers total
    echo !total!
endlocal
goto :eof